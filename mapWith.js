const mapWith=(iterable,fn)=>({
    [Symbol.iterator]:
    function* (){
        for(let element in iterable){
            yield fn(element);
        }
    }
});


let some=mapWith({one:1,two:2,car:5},x=>x);
console.log(...some);